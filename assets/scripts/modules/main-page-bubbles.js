mp_bubble_init();
$(document).on( 'aprel:resize', mp_bubble_init );

// $('.mp-bubble__bg:not(.mp-bubble_active)').click(function () {
// 	bbls_show_as_default();
// 	bbls_bbl_active($(this));
// });

$('.mp-bubble__bg:not(.mp-bubble_active)').click(function (e) {
	e.stopPropagation();
	bbls_show_as_default();
	bbls_bbl_active($(this));
});

$('.mp-bubble__close').click(function (e) {
	e.stopPropagation();
	bbls_show_as_default();
});

function bbls_show_as_default() {
	$('.mp-bubble__bg').removeClass('mp-bubble_active').removeClass('has-transition');
	$('.mp-promo__bubbles__bg-layer').removeClass('bbl-active');
}
function bbls_bbl_active(el) {
	el.addClass('has-transition').addClass('mp-bubble_active');
	el.parents('.mp-promo__bubbles__bg-layer').addClass('bbl-active');
}

function mp_bubble_init() {
	if ($(window).width() >= 1200) {	
		$('body').mousemove(function (e) {
			var l1 = $('.js-bubble-parallax-layer-1');
			var l2 = $('.js-bubble-parallax-layer-2');
			var l3 = $('.js-bubble-parallax-layer-3');
			var l4 = $('.js-bubble-parallax-layer-4');
			var l5 = $('.js-bubble-parallax-layer-5');
			var l6 = $('.js-bubble-parallax-layer-6');
			parallax(e, l1, 1);
			parallax(e, l2, 2);
			parallax(e, l3, 3);
			parallax(e, l4, 2);
			parallax(e, l5, 1);
			parallax(e, l6, 2);
		});
		mp_bubble_container_size();
		mp_bubble_size();
	}
}

function mp_bubble_container_size() {
	var container = $('.mp-promo__block__content');
	$('.mp-promo__bubbles__bg-layer').each(function () {
		var w = $(this).width();
		$(container).height(w/1.5);
	});
}

function mp_bubble_size() {
	$('.mp-bubble__bg').each(function () {
		var data_styles = $(this).data('styles'),
		size_coeff = data_styles.width.slice(0, -1) / 100;
		var w = $(this).parent().innerWidth() * size_coeff *.9;
		
		var fsz = w / 15;

		$(this).css(data_styles);
		$(this).css({
			'height': w,
			'font-size': fsz > 10 ? fsz : 10 + 'px'
		});
		$(this).width(w);
		$(this).height(w);
	});
}

function parallax(e, target, layer) {
    var layer_coeff = 10 / layer;
    var x = (e.pageX - $(window).width() / 2) / (5 * layer_coeff);
    $(target).css({ 
    	'transform': 'translateX(' + x + 'px)',
    	'-webkit-transform': 'translateX(' + x + 'px)',
    	'-moz-transform': 'translateX(' + x + 'px)'
    });
};
