<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';

  $wp_customize->add_setting('upload_logo');
  $wp_customize->add_control(
    new \WP_Customize_Image_Control(
      $wp_customize,
      'upload_logo',
      array(
        'label' => 'Логотип мобильной версии',
        'section' => 'title_tagline',
        'settings' => 'upload_logo',
        'transport' => 'postMessage'
      )
    )
  );

  $wp_customize->add_section(
      'left_sidebar_content',
      array(
          'title' => 'Содержимое левого сайдбара',
          'priority' => 35,
      )
  );
  $wp_customize->add_setting(
    'google_play_link',
    array(
      'default' => '#',
      'sanitize_callback' => 'sanitize_text_field',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'google_play_link',
    array(
      'label' => 'Ссылка на Google Play',
      'section' => 'left_sidebar_content',
      'settings' => 'google_play_link'
    )
  );
  $wp_customize->add_setting(
    'facebook_link',
    array(
      'default' => 'https://facebook.com/aplaprel',
      'sanitize_callback' => 'sanitize_text_field',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'facebook_link',
    array(
      'label' => 'Ссылка на facebook',
      'section' => 'left_sidebar_content',
      'settings' => 'facebook_link'
    )
  );
  $wp_customize->add_setting(
    'facebook_access_token',
    array(
      'default' => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'facebook_access_token',
    array(
      'label' => 'Access token для доступа к facebook',
      'section' => 'left_sidebar_content',
      'settings' => 'facebook_access_token'
    )
  );


  // Шарики
  $wp_customize->add_section(
      'header_bubbles',
      array(
          'title' => 'Шарики с изображениями',
          'priority' => 45,
      )
  );

  // Четыре стильных шарика ;)
  $wp_customize->add_setting('custom_bubbles');
  $wp_customize->add_control(
    new \WP_Customize_Image_Control(
      $wp_customize,
      'custom_bubbles',
      array(
        'label' => 'Четыре стильных шарика ;)',
        'section' => 'header_bubbles',
        'settings' => 'custom_bubbles',
        'transport' => 'postMessage'
      )
    )
  );
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');
