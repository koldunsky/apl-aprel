<?php 
use Roots\Sage\Extras;
?>

<?php while (have_posts()) : the_post(); ?>
	<?php
		// extra fields
		$ev_dt = array(
			'event_date_start' 	=> get_field('event_date_start'),
			'event_date_end' 		=> get_field('event_date_end'),
			'event_time_start' 	=> get_field('event_time_start'),
			'event_time_end' 		=> get_field('event_time_end')
		);
		$ev_d_start = new DateTime($ev_dt['event_date_start']);
		$ev_d_end = new DateTime($ev_dt['event_date_end']); 
		$location = get_field('event_loc');
	?>
  <article <?php post_class(); ?>>
    <header class="page-header">
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>
      
			<div class="row">		
				<div class="col-md-4 col-sm-12 event-info">

					<div class="event-info__block">					
						<h4><span class="dashicons dashicons-calendar-alt"></span>Время проведения:</h4>
							<?php 
								if ($ev_d_start == $ev_d_end || empty($ev_dt['event_date_end'])) {
									echo Extras\getRusDate($ev_dt['event_date_start']);
								} else {
									echo Extras\getRusDate($ev_dt['event_date_start']);
									echo ' — ';
									echo Extras\getRusDate($ev_dt['event_date_end']);
								}
								if($ev_dt['event_time_start']) {
									echo '<br>c '.substr($ev_dt['event_time_start'],0,5).' ';
								} 
								if($ev_dt['event_time_end']) {
									echo 'до '.substr($ev_dt['event_time_end'],0,5);
								}
							 ?>
					</div>

					<?php if (get_field('event_price')): ?>
					<div class="event-info__block">	
						<h4><span class="dashicons dashicons-tickets-alt"></span>Стоимость:</h4>
							<?php the_field('event_price'); ?>
					</div>
					<?php endif ?>					


					<?php if (get_field('experts_on_event')): ?>
						<div class="event-info__block">	
							<h4><span class="dashicons dashicons-welcome-learn-more"></span>Ведущие:</h4>
								<?php $experts = get_field('experts_on_event'); ?>
								
								<?php if($experts):	 ?>							
									<?php foreach ($experts as $expert): ?>
										<div class="author-badge">
										  <?php $author_img = types_render_field('expert_avatar', array('id'=>$expert -> ID, 'url'=> true)); ?>
										  <div class="author-badge__img" style='background-image: url(<?php echo $author_img; ?>)'>
										  	
										  </div>
										  <div class="author-badge__name">
										  	<a href="<?php echo $expert->guid ?>">
										  		<h5>  			
										  			<?php echo $expert->post_title ?>
										  		</h5>
										  	</a>
										  </div>
										</div>
									<?php endforeach; ?>
								<?php endif;?>	
						</div>
					<?php endif ?>					
				</div> <!-- .col-md-4 -->

				<div class="col-md-8 col-sm-12">
					<div class="row">	
						<?php 
							if( !empty($location) ):
							get_template_part('templates/elements/acf-gmap'); 
						?>
						<div class="col-xl-12">
							<div class="acf-map__address">
							<?php if(has_post_thumbnail()): ?>
								<div class="acf-map__cover" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?> )"></div>		
							<?php else: ?>								
								<div class="acf-map__cover" style="background-image: url(/wp-content/themes/aprel/static/img/numbers-wallpaper-1920x1200-1024x640.jpg)"></div>	
							<?php endif; ?>
								<h4><span class="dashicons dashicons-location"></span>Место проведения:</h4>
								<p>
									<?php echo(str_replace("г. Москва, город Москва,", "г. Москва,", $location['address'])); ?>
								</p>
								</p>
							</div>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
						</div>
						<?php endif; ?>
					</div> <!-- .row -->
				</div> <!-- .col-md-8 -->
			</div> <!-- .row -->
			<div class="entry-content event-form">
			<h3>Запись на событие &laquo;<?php the_title(); ?>&raquo;</h2>
				<?php echo do_shortcode("[contact-form-7 id='386' title='Cобытие']"); ?>
			</div>

			<?php if(strlen(get_the_content())): ?>
				<div class="entry-content">
					<?php the_content(); ?>
				</div> <!-- /.entry-content -->
	  	<?php endif; ?>

			<?php if(get_field('event_report')): ?>
				<div id="event-report" class="entry-content event-report">
					<h1>Отчет о мероприятии<a href="<?php echo get_permalink(); ?>#event-report" class="dashicons dashicons-paperclip js-copy-link"></a></h1>
					  <?php the_field('event_report'); ?>
				</div><!-- /.event-report -->
	  	<?php endif; ?>			

    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
