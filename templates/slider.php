<?php 
use Roots\Sage\Extras;
?>

<article 
  <?php post_class('aprel-post-slider-item owl-lazy'); ?> 
  <?php if(has_post_thumbnail()) : ?>
  	data-src="<?php the_post_thumbnail_url( 'full' ); ?>" 
	<?php else: ?>
  	data-src="/wp-content/themes/aprel/static/img/numbers-wallpaper-1920x1200-1024x640.jpg" 
	<?php endif; ?>
  >
  <header>
  	<?php if(get_field('event_date_start')): ?>
			<h3>
				<?php  
				$ev_start = new DateTime(get_field('event_date_start'));
				$ev_end = new DateTime(get_field('event_date_end'));

				echo $ev_start->format('d.m.Y');

				if (get_field('event_date_end') && $ev_start != get_field('event_date_end')) {
					$ev_end = new DateTime(get_field('event_date_end'));
					echo ' — '.$ev_end->format('d.m.Y');
				}
				?>
			</h3>
  	<?php endif; ?>
    <h2 class="entry-title aprel-post-slider-item__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php //the_excerpt(); ?>
    <a href="<?php the_permalink(); ?>">
    	<button class="btn btn-white aprel-post-slider-item__btn">Подробнее</button>
  	</a>
  </header>
</article>