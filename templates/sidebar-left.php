<section class="sidebar-left col-sm-3">
	<?php 
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	 ?>
	<div class="sidebar-left__logo">			
		<a href="<?= esc_url(home_url('/')); ?>">
			<img src="<?php echo $image[0]; ?>" alt="<?php get_bloginfo('name') ?>" class="sidebar-left__logo__desktop">
			<img src="<?php echo esc_url( get_theme_mod( 'upload_logo' ) ); ?>"  alt="" class="sidebar-left__logo__mobile">			
		</a>
	</div>
	<div class="sidebar-left__social">
 		<?php get_template_part('templates/elements/soc-btns'); ?>
	</div>
	<nav class="nav-left-sidebar">
    <?php
    if (has_nav_menu('sidebar-left_navigation')) :
      wp_nav_menu(['theme_location' => 'sidebar-left_navigation', 'menu_class' => 'nav menu-container']);
    endif;
    ?>
  </nav>
  <nav class="nav-tablet nav-small-screen">
    <?php
    if (has_nav_menu('tablet_navigation')) :
      wp_nav_menu(['theme_location' => 'tablet_navigation', 'menu_class' => 'nav']);
    endif;
    ?>
  </nav>
</section>