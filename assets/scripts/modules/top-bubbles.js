// Bouncing Balls. By Rob Glazebrook
// The balls are randomized in size, color, opacity, and bounce direction. They'll bounce off the walls of their container and generally make a rather pretty show of things.
var  styles = {
      color: [
        '#E85433', //red
        '#88BC24', // green
        '#FAB900', //yellow
        '#FAB900' //yellow
      ],
      fill: [
         true,
         true,
         true,
         false // stroke
      ],
      bg: ["url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCQzYwOERGNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCQzYwOEUwNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0JDNjA4REQ3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0JDNjA4REU3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz76LNtPAAAAuElEQVR42mL8//8/w2AGTAyDHIw6cNg7kIWKZgkCcRoOuU5yDWWkUi4uh2JBXPYMVAgaA/FMKD2o0iAopDqA+AwtHUduCKZBHSc42DKJMdRhLoMxF4MctXswl4N7oBif/KyBziTpQPweTeweEIcBsSuUPaAOvIdW4ILYJkC8ejDl4k5o7gU56uxgreoqRhsLow4cdeCoA0cdOOrAUQeOOnDUgaMOHHUgFDCOjrCOOnDUgUPcgQABBgCv0By7GoWpmQAAAABJRU5ErkJggg==')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCQzYwOEUzNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCQzYwOEU0Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0JDNjA4RTE3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0JDNjA4RTI3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7m53KiAAABfElEQVR42uxY7W3DIBC1qyxAR6AjeAVWSEdwR3BHaEZwRkhGiEeIR4hHcEegh3SR+OHAwQG2Kp/0ZPwBPL0D37NrrXW15XirNh47wX9P8JBgDAk4AhS2JV4fABNgBFwBv1Gjm10cCQnoNT167BM0Tyy5FjDr8DB9jrkJKs2PtoSCnJip6easQXv9PQCdNWmD57NnTXrnqZmlrsfj94td2gDujv7v3t3NUJCKi0NF5etf4kU9br2SyK0TVI5709oEW4eC49oEBeDHcf9MGaTO5KgNuRu+Zl6l9mOtNegjZ+KrhJtZQoNVJUkd5pa6GIfTlrJbNgTBFwbbrFQEKSm943NVaYId0UULjgixVv+WK6Vcgh1hI1y4qsUS9Kn2SKVaDEFJJCdSE6SWOoXVgRITYrDa0Z6Q+uHeBPo/uWCzbMKnNQn6fOE1h4InTJNEshzCQ8jDHLv1JCpQnWfbF/4vuYx+UFjEl9Q26f0spWCM2lPJFO8/MHeCKeJPgAEAKPDciNynX+gAAAAASUVORK5CYII=')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCRTEzNkMxNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCRTEzNkMyNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0JDNjA4RTU3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0JDNjA4RTY3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Isji4AAABhElEQVR42uxY7Q2CMBAF4wJ1BBwBR2AFHAFHwBFcAUfQEWAEHUFGwBHqmZxJ06RfV9oSwyXvhxXo4z7eXck559mSbZMt3FaCK0GD1YAO8ABwCRPgBmi8dvhWMQEtYOL29gCUlL1cbyhxM4pNFJK5gw6WgB7APAL2BBxC5OAc5H7PqUMVyahYOwP2gBxxQE/pSAYpEiblX4drqmtVRdS75ODW4V3e6J0Of58M1969JQZsS7jn5JESmSH8UTuJKteGGEJtQjVH/lGE2kbIW003YakI9oYu0lHIfRFjmrliwVQxhwUZL4d+3Ibqxdr3JHjVSq7mItijvr2FtcoQ1iOKeRKZEedG3YwYLcQ6ewEKxX87yetJziSDz2QTg+C49FMdS0WwsiTXeHnXo+f+mn9hOeBGq2KGlSmG7o46OGLFFnj2YCl0kHrslIeHINNMMwO5m8tkQ/Fg4/hVQbQ21jxYYJgmywnmoimm4K2uxqJg2BlGQT4G10NSqGlm/T64Evxbgh8BBgCkZ+mCjHowmAAAAABJRU5ErkJggg==')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCRTEzNkM1Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCRTEzNkM2Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0JFMTM2QzM3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0JFMTM2QzQ3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7R+4YiAAABiElEQVR42uyY0ZGDIBCG9SYNcCXYAlcCLXgleCWYEnIl5EowJcQSTAlJCaYEAnObmYyBhZV18MF/ZicPGPiEfxek1FoXa9ZHsXJtgGsEVCY6E4KlN5skjFGZGPW/7K9K7ZMbcNDvOpgQawA8aL+G3ICNxqVyAsoX37nU5vSg8PjuqS53knQI3DUlOTgA24DvJIe/U3yHqeGqDnN9hyXFkbO2zvnTOVDvRE5AzHfjxHcKkqhN8SPl4Trgu3ry/NHxAh0Vds4hQHv221ifsgOGivFA2PpG6hLHnAdrE9LTdjfx7TkTutQvcWCVSJsFbCZAFbyUSxcqYBnxVXeFQWPUA7QP8IsKGQKsAJBDFvxziSX+nbM0DtlvlLOJNmAb8hK/DqCgc0UZxKGTJ7netCMu0QkiFfiyxAzGzPAQmVDRycINOHIny67gk0LafqAEKeoHPSegDNTHm4m/nFcfvhm8QWS9mxHIDPYpHXPO4B5K0D11/10qi6d+fNbGfcoSl9sV8Aa4AeJ6CDAAxry9pQ6NedcAAAAASUVORK5CYII=')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCRTEzNkM5Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCRTEzNkNBNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0JFMTM2Qzc3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0JFMTM2Qzg3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6fV2Q6AAABkElEQVR42uxYi22DMBDFVRagI7gjpCPQEbICHYGMQEfICu0I9QgwAowQRnBj6SydInz4k3MthSedRDDiXny/Z4TWuioZL1Xh2Amm4sD8/iOYhN/n/yQoEZkGrmu0rnLuYA0kXGSyh9g4b1dCxg7h2QcNqSHR13yzNy6CBjqC0HhnC2cOKsi3NSxAQCFic+42MzoIGnKvJTTqmSggWQLBcaOIiiC4EE26iFns2sUGhbu72S9UvTXTovqoP2LaTIB1eh1XWLtqGma9DfEZSvCkHwNvkiJQUZsQTQ9Kr/eNwovKwTlmGjjQcwnWkbj/CU1bwA5REqvxak+BObhWKLZAXM8PRC72W/4OkTv4BeFWHjPXPPsd2+BFpmMn5USUcGhSuSbJ0x07lxIISkLoKm6CLeHciodLUm5G9EFrRyQOLjCna1iTMG8nogcOHLMYY0gQqib3PjhmMZ6jKSr67EMuNsT1RugoTJAabHoQk/QRqJhYh3K04tKDa1V6QspE3p2Trf1wf/rYP2DuBJ+W4J8AAwDaqnXjo8zn7QAAAABJRU5ErkJggg==')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdDMDQ3Qzk0Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdDMDQ3Qzk1Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0JFMTM2Q0I3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0MwNDdDOTM3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/M6Q7AAABpElEQVR42uxY7Y2DMAyF6hZgBVbICukI7QgwAiv0RoAR2hHaEWCEMgKMkIslI0VV8+UkXH5g6UkISGI9+9lJSiFEkbOdisztcDDUfiLOxSWY8jwreEmspFlBJAGoJDqJRdito6wRwuBFopeoHP+f92SwEX72pkbpFMCcj/1Sw1R6FmoI51sTVhDBgIIA1CgaEExLTiRPyntNCEEkLFBwX+HDILC2aL61yN6/FuqLQZ1DDp2Ea96/cukk3KG+1cg0x2dIiwkxkGqhY7LWhhrH8Z9bim7iymBtEc+o9GGT3XCuNjaD3MDKKPytS9lJPo0RxnSuPTzmfhCEcIXuhDjjO11aNHuFeAtz9WUcs4yxru3qoGmhRePcBlOORsvByaJi0275QaitpByciGVo3qvVTUQm1hwcZEUq8zwgmYSiG9ftIZItVA9CXeME8ZB31NzCIvM4XDUx66CKu6Vo3zGsY2iR9t3yuxycXFPlbBFdUC/eFlhTOxd69cHwQO5qT9z4eq0T625mNAinV3bdSY+dLrnJPgr7GjppeVwBHw4eDprtT4ABAPr5egS2FbP3AAAAAElFTkSuQmCC')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdDMDQ3Qzk4Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdDMDQ3Qzk5Nzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0MwNDdDOTY3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0MwNDdDOTc3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7YtwYtAAABMElEQVR42uyYwQ2DMAxFIeoCrNCM0BVgBDpCVmCFskJW6Ah0hY4AKzBCSiS4oNrUwSaoiiVfCLK+XmzHSe6cy85sKju5JYF/L/BC+LeY/Pbl+zB7dIFm8gcx/gv4XkkIDLEyFYmAvaVycARy6jo7Jc7v5o86AS8dbA0lVi50FndAgXh6mkJRCVUuVL2WusUSBDtEoKY2dXUwvSF2mzHIWhu7D/pWU3PS4xbYIGvP0KBcReLp9cjAUIUGVgfQa/cE5iAoRo+LoJHIPS6CxUyvACZtHXvcMoC43bnHQVCc3l6CGD3L1VxDCWL0yCOVBMF6I/fG2AR7YMxnpRdK0CB3EMspLpRgjwgkD6TcBOsNeuxPICEEl7lvvdX6LALXI/5S0XeJ62GenoCTwCQQt48AAwBpR9dPa4wy+QAAAABJRU5ErkJggg==')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdDMDQ3QzlDNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdDMDQ3QzlENzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0MwNDdDOUE3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0MwNDdDOUI3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5RmRNKAAABpElEQVR42uxYi42DMAyFqgvkRuBGYAU6AiuwAjcCHYGOcDcCjNCOQEeAEXKO5ErVSXFix/RQhaWnShThp/jz7OTW2mzLdsg2bjvBtyd4VPpODagABaAELIA74IYY8RnfXBUnoARcbdhmQA8ouD5Syc2Wby3HT57QBycMqcRc2E8xYZcWSZNALsM8HQBmLYIV8d8F8AHI8ffLc1KOZLtWkfhyb/C8XxE5SRaO9AR9ofnxPB8x73wtSj3EPmf3rSjJyMzNRwN/GcELs7ob4lvLWkrSeZLeKYt5eq8NKIyh/ByFBVIGetwVTznUL8+aJ+ik7dvqWa+pxY0isZmjxzEv1RFOp8BU8yBVrzHNTAHHTWRBNJJiTDm9GfOSkw6lNsFOONe1RAtSJTgQBI0wNVh5mKLFi1BtKk2pG4lmLJ14ilcQDOlrcIzSIngjSHYEiTZxJWBJXRHY3gZsLRWi/4+tLnrJCZgrrE/OEn9gVO0pk94OcKaXxHnQRIQwaXrRulko0OEcOUjU0sE45WbhufGWCPMnLW7EphdlGgT3C8yd4FsT/BVgADKHkO3rlbpTAAAAAElFTkSuQmCC')",
         "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdDMkIxRTUxNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdDMkIxRTUyNzg0NTExRTZBQkMwQjg2OTg2OTcwMzc4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0MyQjFFNEY3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0MyQjFFNTA3ODQ1MTFFNkFCQzBCODY5ODY5NzAzNzgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4CNreGAAABf0lEQVR42uyY7Y3DIAyGk+oW4EZIR6Aj0BGyQlZoR+gKyQhdIazQEdIRwgicI7lSdToMGIiqE5beX1HJU/z6Q2mttc0nx6H58KiAFTAwetAdtILsmxbQiM95sVVxgiRotmGxgIbYd6TCrTY+tj8kSgNy4V6x4hned7XMRj2DVKJvn6ATyOQuksEDp0FHUAv6Bl0dEB3oUqJI7h5/uSzhSrXI6UHh8VZH/NZV7WRlx6ZYEs8m9FVsiJwelB7vcUDUXqNOe+DkHqPuQTwznlHIuvkDo3dR7YcDaHK3mTlyOvQJlc+aJAonCZUyjb7rsSFTljmV2GZuNk8MJbeZSyLcUnrdatA/o8OPc+rtpWwzVMM1bxuPILrBcY+N+i+pgF1R7bFRh6b7d9xizg1J8Yit4uFoqgLTKwPH4TnGNyGAi6eXxYzJs3dyMEZdDriJA7fFV2E4gyv/xD2gFOAToSbOrcV68NXjOiwI126nEUYzN+skwPrxqAJWwAr4XwF/BBgAFxn/Ab8ZtMwAAAAASUVORK5CYII=')"
         
      ]
   };
var ballMinSize = 10, 
    ballMaxSize = 50,
    container = $('#header-bubbles:visible'),
    ballCount = Math.round(container.width() / 50);

$(function() {
  initBalls();
  balls = window.setInterval(moveBalls,40); // 24 FPS
  $(window).resize(function() { moveBallsIntoBounds(); });
});

// Random number generator. Takes a minimum, maximum, and a boolean for whether the random number should be an integer.
function rand(min,max,isInt) {
  var min = min || 0, 
      max = max || 1, 
      isInt = isInt || false,
      num = Math.random()*(max - min) + min;
  return (isInt) ? Math.round(num) : num;
}

function setNumBg() {
   var bgArrIndex = Math.round(rand(0,styles.bg.length-1, true));
   var  bgImg = styles.bg[bgArrIndex];
        styles.bg.splice(bgArrIndex,1);
   return bgImg;
}

// Creates the balls, puts them in the container, and gives them a random size, color, opacity, starting location, and direction/speed of movement.
function initBalls() {
  container.css({'position':'relative'});

  var extraBallsSettings = {
    color: ['#88BC24', '#E85433', 'transparent', 'transparent'],
    border: ['none', 'none', '2px solid #FAB900', '2px solid #88BC24'],

  };

  console.log(ballCount);

  if(top_bubble_imgs) {  
    for(var i=0; i < Math.ceil(ballCount / 2.5); i++) {
      // top_bubble_imgs
      var j = extraBallsSettings.color[i]  ?  i : i%4;
      var extraBall = $('<div />', {
        'class': 'banner-bubble'
      }).appendTo(container);
      extraBall.css({
        'position':'absolute',
        'width': '45px',
        'height': '45px',
        'top': rand(0,container.height()-45),
        'left': rand(0,container.width()-45),
        'opacity': .8,
        'background-color': extraBallsSettings.color[j],
        'background-image': 'url(' + top_bubble_imgs + ')',
        'background-size': '180px 45px',
        'background-position': j * -45 + 'px center',
        'background-repeat': 'no-repeat',
        'border': extraBallsSettings.border[j],
        'box-sizing': 'content-box '
      }).attr({
        'data-dX':rand(-4,4),
        'data-dY':rand(-2,2)
      });
    }
  }

  for (var i=0; i<ballCount; i++) {     
    var newBall = $('<div />',{
      'class': 'banner-bubble'
    }).appendTo(container),
      size = Math.round(rand(ballMinSize,ballMaxSize)),
      fill = styles.fill[Math.floor(Math.random()*styles.fill.length)],
      color = styles.color[Math.floor(Math.random()*styles.color.length)],
      filled = {
        'background-color': color
      },
      stroked = {
        'border-width': '2px',
        'border-style': 'solid',
        'border-color': color
      },
      imaged = {
        'background-image': bgImg
      },
      bgImg;
     
     
     if(fill && size > 30 && styles.bg.length > 0) {
        bgImg = setNumBg();
        newBall.css(filled);
         newBall.css(imaged);
     } else if(size < 30) {
       newBall.css(filled); 
     } else {
       newBall.css(stroked); 
        bgImg = "none";       
     }
    newBall.css({
      'position':'absolute',
      'width': size+'px',
      'height': size+'px',
      'opacity': .8,
      'top': rand(0,container.height()-size),
      'left': rand(0,container.width()-size)
    }).attr({
      'data-dX':rand(-4,4),
      'data-dY':rand(-2,2)
    });
  }
}

// Moves the balls based on their direction/speed of movement (saved as a data attribute). If the movement will take them outside of the container, they reverse direction along that axis.
function moveBalls() {
  var maxX = container.width(),
      maxY = container.height();
  $('.banner-bubble',container).each(function(i,b) {
    var ball = $(b),
        pos = ball.position()
        x = pos.left,
        y = pos.top,
        dX = parseFloat(ball.attr('data-dX')),
        dY = parseFloat(ball.attr('data-dY')),
        size = ball.height();
    if(x+dX+size > maxX || x+dX < 0) ball.attr('data-dX',(dX=-dX)); 
    if(y+dY+size > maxY || y+dY < 0) ball.attr('data-dY',(dY=-dY)); 
    ball.css({'top':y+dY,'left':x+dX});
  });
}

// Move the balls back within the bounds of the container if the window (ergo, possibly the container) is resized. Because we're positioning from the top/left corners, we only have to worry about the bottom/right sides.
function moveBallsIntoBounds() {
  var maxX = container.width(),
      maxY = container.height();  
  $('.banner-bubble',container).each(function(i,b) {
    var ball = $(b),
        pos = ball.position()
        x = pos.left,
        y = pos.top,
        size = ball.height();
    if (x+size > maxX) ball.css({ left: maxX-size+'px' });;
    if (y+size > maxY) ball.css({ top: maxY-size+'px' });;    
  });
}