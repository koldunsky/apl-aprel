<?php
/**
 * Template Name: Contacts Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php if (get_field('contacts-map')) : ?>
		<div class="row">
			<div class="col-md-5 col-xxl-5">
				<?php the_content(); ?>
			</div>
			<div class="col-md-7 col-xxl-7">
				<?php $location = get_field('contacts-map'); ?>
				<?php get_template_part('templates/elements/acf-gmap'); ?>
				<div class="acf-map">
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					</div>
			</div>
		</div>
	<?php else : ?>
  	<?php the_content(); ?>
	<?php endif; ?>
<?php endwhile; ?>