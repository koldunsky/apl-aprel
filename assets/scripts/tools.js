		var aprel = {};
		"use strict";

		var userAgent = window.navigator.userAgent;
		var mobAppleDevice = false;

		if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
		   mobAppleDevice = true;
		}
		/*------------------------------------------------------------
		 * Helpers
		 *------------------------------------------------------------*/

		/* Window width and device
		 *------------------------------------------------------------*/

		function resolutionUtilities(){

			// get window width & height
			aprel.wW  = $(window).width();
			aprel.wH = $(window).height();

			// get device by window width
			if( aprel.wW <= 768 ){
				aprel.device = 'mobile';
			}

			if( aprel.wW > 768 && aprel.wW <= 992 ){
				aprel.device = 'tablet';
			}

			if( aprel.wW >= 992){
				aprel.device = 'desktop';
			}

		}

		resolutionUtilities();
		$(document).on( 'aprel:resize', resolutionUtilities );


		/*------------------------------------------------------------
		 * EVENT: aprel:Resize
		 *------------------------------------------------------------*/

		var resizeTimeout;
		$(window).resize(function() {
			requestAnimationFrame(function() {
				clearTimeout(resizeTimeout);
				resizeTimeout = setTimeout(function() {
					$(document).trigger('aprel:resize');
				}, 700);
			});
		});

function goToByScroll(el) {
  $('html,body').animate({scrollTop: $(el).offset().top},'500');
}

// set up a moment.js
moment.locale('ru');