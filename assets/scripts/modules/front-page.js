;
var owlPostSlider = $('.aprel-post-slider');

owlPostSlider.on('initialized.owl.carousel', function(event) {
    progressBar(event);
});
owlPostSlider.on('translate.owl.carousel', function(event) {
    moved();
});
owlPostSlider.on('drag.owl.carousel', function(event) {
    pauseOnDragging();
});

var time = 7; // time in seconds

var $progressBar,
  $bar, 
  $elem, 
  isPause, 
  tick,
  percentTime;

//Init progressBar where elem is $("#owl-demo")
function progressBar(elem){
  $elem = $(elem.target);
  //build progress bar elements
  buildProgressBar();
  //start counting
  start();
}

//create div#progressBar and div#bar then prepend to $("#owl-demo")
function buildProgressBar(){
  $progressBar = $("<div>",{
    class:"aprel-post-slider__progressBar"
  });
  $bar = $("<div>",{
    class:"aprel-post-slider__bar"
  });
  $progressBar.append($bar).prependTo($elem);
}

function start() {
  //reset timer
  percentTime = 0;
  isPause = false;
  //run interval every 0.01 second
  tick = setInterval(interval, 10);
};

function interval() {
  if(isPause === false){
    percentTime += 1 / time;
    $bar.css({
       width: percentTime+"%"
     });
    //if percentTime is equal or greater than 100
    if(percentTime >= 100){
      //slide to next item 
      $elem.trigger('next.owl.carousel');
    }
  }
}

//pause while dragging 
function pauseOnDragging(){
  isPause = true;
}

//moved callback
function moved(){
  //clear interval
  clearTimeout(tick);
  //start again
  start();
}

//uncomment this to make pause on mouseover 
owlPostSlider.on('mouseover',function(){
  isPause = true;
})
owlPostSlider.on('mouseout',function(){
  isPause = false;
})

owlPostSlider.owlCarousel({
    loop:true,
    responsiveClass:true,
    lazyLoad: true,
    smartSpeed:450,
    dots: true,
    navText: ['&larr;','&rarr;'],
    responsive:{
        0:{
            items:1
        }, 
        767:{
            items:1,
            nav:true
        },
        1600:{
            items:2,
            nav:true
        }
    }
});