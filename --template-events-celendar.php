<?php
/**
 * Template Name: Events Calendar
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

  <?php 
  //$events = events
  $events_args = array(
		'posts_per_page'   => 5,
		'offset'           => 0,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'events',
		'post_status'      => 'publish'
	);

	$events_posts_raw = get_posts($events_args);
	$events_posts = array();
	//print_r($events_posts_raw);

	function buildEvent($event_new, $day, $startTime, $endTime) {
		$event_new['start'] = $day.'T'.$startTime;
		$event_new['end'] = $day.'T'.$endTime;
		return $event_new;
	}

	foreach( $events_posts_raw as $event_raw ){		
		$ev_id = $event_raw->ID;
		$ev_dt = array(
			'event_date_start' 	=> get_field('event_date_start', $ev_id),
			'event_date_end' 		=> get_field('event_date_end', $ev_id),
			'event_time_start' 	=> get_field('event_time_start', $ev_id),
			'event_time_end' 		=> get_field('event_time_end', $ev_id)
		);
		$ev_d_start = new DateTime($ev_dt['event_date_start']);
		$ev_d_end = new DateTime($ev_dt['event_date_end']); 
		$ev_t_start = $ev_dt['event_time_start'];
		$ev_t_end = $ev_dt['event_time_end']; 

		$event_new = array(
			'id' => $event_raw->ID,
			'title' => $event_raw->post_title,
			'url' => get_permalink($ev_id),
			'price' => get_field('event_price', $ev_id)
		);

		if ($ev_d_start > new DateTime()) {
			$event_new['color'] = "#88BC24"; //green
			$event_new['class'] = 'event_future';
		} else {
			$event_new['color'] = '#E85433'; //orange
			$event_new['class'] = 'event_past';
		}

		if ($ev_d_start == $ev_d_end) {
			$events_posts[] = buildEvent($event_new, $ev_d_start->format('Y-m-d'), $ev_t_start, $ev_t_end);
		} else {
			$day_index = 1;
			for ($i = $ev_d_start; $i <= $ev_d_end; $i->add(new DateInterval('P1D'))) {
				$event_new['title'] = $event_raw->post_title." - день ".$day_index;
				$day_index++;
				$events_posts[] = buildEvent($event_new, $i->format('Y-m-d'), $ev_t_start, $ev_t_end);
			}
		}

	}	
	
	
	echo '<script>';
	echo('var eventsArr = '.json_encode($events_posts));
	echo '</script>';
  ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.js"></script>

<div id="fullcalendar"></div>
<script>
(function($) {
	$(document).ready(function() {
		console.log(moment());
		eventsArr.url = encodeURIComponent(eventsArr.url);
		$('#fullcalendar').fullCalendar({
			height: 'auto',
			contentHeight: 'auto',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'listMonthCustom,monthCal'
			},
			views: {
				listMonthCustom: {
					type: 'listMonth',
					duration: { months:  1},
					title: 'Лист',
					buttonText: 'Лист'
				},
				monthCal: {
					type: 'month',
					duration: { months:  1},
					title: 'Календарь',
					buttonText: 'Календарь'
				}
			},
			defaultView: 'listMonthCustom',
			slotLabelFormat: 'H:mm',
			minTime: '08:00:00',
			maxTime: '22:00:00',
			//navLinks: true, // can click day/week names to navigate views
			businessHours: {
				// days of week. an array of zero-based day of week integers (0=Sunday)
				dow: [ 1, 2, 3, 4, 5 ], // Monday - Thursday

				start: '08:00', // a start time (10am in this example)
				end: '22:00' // an end time (6pm in this example)
			},
			editable: false,
			events: eventsArr,
			// eventRender: function(event, element, view) {
			// 	return $('<div>' + event.title + '</div>');
			// }
		});
	});
})(jQuery);
</script>
<?php endwhile; ?>