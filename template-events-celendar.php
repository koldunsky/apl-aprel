<?php
/**
 * Template Name: Events Calendar
 */
?>
<div id="clndr-full" class="clndr-full">
</div>


<script id="clndr-full-template" type="text/template">
  <div class='clndr__controls'>
  	<div class="clndr__controls__calendar pull-right">
  		<span class="dashicons dashicons-calendar-alt"></span>
  	</div>
  	<div class="clndr__controls__grid active pull-right">
  		<span class="dashicons dashicons-editor-table"></span>
  	</div>

    <div class='clndr-previous-button pull-left'>&lsaquo;</div>
    <div class='clndr-next-button pull-left'>&rsaquo;</div>
  </div>
  <div class='clndr__month'>
    <h1>
    	<%= month %> 
    </h1> 
  </div>
  <div class="clndr__full__container">
  	<div id="clndr__full__grid" class="clndr__full__grid active">
  		<!-- events grid here -->
  		<div class="preloader-container" style="position: absolute;">	
				<div class="preloader-logo"></div>
				<div class="preloader-bubbles">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
  	</div>
	  <div class='clndr__table' border='0' cellspacing='0' cellpadding='0'>
	    <div class='clndr__table__head'>
	    	<% for(var i = 0; i < daysOfTheWeek.length; i++) { %>
	            <div class='clndr__day clndr__table__head__day'><%= daysOfTheWeek[i] %></div>
	        <% } %>
	    </div>
	    <div class="clndr__table__body">
		    <% _.each(days, function(day) { %>
				    <div class="<%= day.classes %> clndr__day">
				    <% if(typeof day.events !== 'undefined' && !_.isEmpty(day.events)) { %>
				    	<% _.each(day.events, function(event) { %>
					    	<div class="clndr__event">


					    		<div class="clndr__event__time">
					    		<% if(typeof event.startTime !== 'undefined' && !_.isEmpty(event.startTime)) { %>
					    			<%= event.startTime.substring(0, 5) %>
					    			<% if(typeof event.endTime !== 'undefined' && !_.isEmpty(event.endTime)) { %>
					    				-<%= event.endTime.substring(0, 5) %>
					    			<% } %>
					    		<% } else { %>
					    				—
					    		<% } %>
					    		</div>


					    		<div class="clndr__event__tags">				    			
					    			<% if(typeof event.tags !== 'undefined' && !_.isEmpty(event.tags)) { %>
		                    <%= event.tags[0] %>
		                <% } %>
					    		</div>


					    		<div class="clndr__event__title">	
				    				<a href="<%= event.url %>">
				    					<%= event.title %>
				    				</a>
					    		</div>


					    	</div>
					    <% }); %>
				    <% } %>
				    	<div class="clndr__day__num">
				    		<%= day.day %>
				    	</div>				    	
				    </div>
				<% }); %>
	    </div>
	  </div> <!-- .clndr__table -->
  </div>
</script>