<?php 
  use Roots\Sage\Extras;
?>
<?php while (have_posts()) : the_post(); ?>

<?php 
  if (get_field('video_post_link')) {
    $videoLink = get_field('video_post_link');     
    $videoId = '';
    $provider = '';
      if(strpos($videoLink, 'youtu') !== false) {
        // Catch YouTube iframe
        $provider = 'youtube';
        parse_str( parse_url( $videoLink, PHP_URL_QUERY ), $url_GET_values );
        $videoId = $url_GET_values['v'];
      } else if($videoLink && strpos($videoLink, 'vimeo') !== false) {
      // Catch Vimeo iframe
        $provider = 'vimeo';
        $videoId = Extras\get_vimeoId($videoLink);
      }
  }
?>
  <article <?php post_class(); ?>>
    <header class="page-header">
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">

      <?php if(strlen($videoId) && strlen($provider)): ?>
        <div class="row" style="margin-bottom: 40px;">
          <div class="video-post__video col-md-8"> 
            <div 
              class="video-post__video__container <?php echo $provider; ?>-player" 
              data-videoid="<?php echo $videoId; ?>"

              <?php if(has_post_thumbnail()): ?>
                data-background="<?php the_post_thumbnail_url(); ?>"
              <?php endif; ?>
            >
               
            </div>
          <!-- video would be here -->
          </div>
          <div class="col-md-4">
            <?php get_template_part('templates/elements/author-badge'); ?>
          </div>
        </div>
      <?php endif; ?>

      <?php the_content(); ?>      
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>