<?php 
use Roots\Sage\Extras;
 ?>

<?php $postLink = esc_url( Extras\get_link_url() ); ?>

<?php 
  if( has_post_thumbnail() ) {
    $img = get_the_post_thumbnail_url();
  } else {
    $img = '/wp-content/themes/aprel/static/img/numbers-wallpaper-1920x1200-1024x640.jpg';
  } 
?>

<article <?php post_class('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6'); ?>>
<div class="video-post__inner">  
  <div class="video-post__video col-md-6"> 
      <a href="<?php echo $postLink; ?>" target="_blank"> 
        <div class="video-post__video__container">   
            <div style="background-image: url(<?php echo $img; ?>);"></div>      
        </div>
      </a>    
  </div>
  <div class="video-post__content col-md-6">
    <header>
      <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" target="_blank">', $postLink ), '</a></h2>' ); ?>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
  </div>
</div>
</article>
