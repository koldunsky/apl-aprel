<header class="header">
    <div class="row">
      <div class="col-md-12 mobile-menu-holder">
        <div class="col-md-3 col-xl-2 hide-xs hide-sm hide-md hide-lg">
          <div class="header__date">
            <?php 
              $date = new DateTime();
              echo $date->format('j');
             ?>
          </div>
        </div>
        <div class="col-md-12 col-lg-9 col-xl-10">
          <div class="header__banner hide-xs hide-sm hide-md" id="header-bubbles">
            <!-- bubbles here ../assets/modules/top-bubbles.js -->
          </div>
          <nav class="nav-primary menu-holder">
            <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
            <div class="nav-primary__extra-bottom-menu">      
              <?php
              if (has_nav_menu('sidebar-left_navigation')) :
                wp_nav_menu(['theme_location' => 'sidebar-left_navigation', 'menu_class' => 'nav menu-container']);
              endif;
              ?>
            </div>
            <div class="soc-btns">      
              <?php get_template_part('templates/elements/soc-btns'); ?>
            </div>    
          </nav>
        </div>
      </div>
    </div> <!-- row -->

    <div class="header__cal hide-xs hide-sm hide-md hide-lg">
      <div class="col-md-3  col-xl-2">
        <div id="header__clndr" class="header__clndr">
          <!-- clndr here -->
        </div>
      </div>
      <div class="col-md-9  col-xl-10">
        <div class="header__cal__bar">
          <!-- events_header_navigation -->
          <?php
          if (has_nav_menu('events_header_navigation')) :
            wp_nav_menu(['theme_location' => 'events_header_navigation', 'menu_class' => 'header__cal__nav col-md-6 ']);
          endif;
          ?>
        </div>
        <div id="header__cal__grid" class="header__cal__grid row">
          <!-- events grid here -->
        </div>
      </div>
    </div> <!-- /header__cal -->  
</header>