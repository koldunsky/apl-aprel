<?php 
use Roots\Sage\Extras;
 ?>

<?php 
  if (get_field('video_post_link')) {
    $videoLink = get_field('video_post_link');     
    $videoId = '';
    $provider = '';
      if(strpos($videoLink, 'youtu') !== false) {
        // Catch YouTube iframe
        $provider = 'youtube';
        parse_str( parse_url( $videoLink, PHP_URL_QUERY ), $url_GET_values );
        $videoId = $url_GET_values['v'];
      } else if($videoLink && strpos($videoLink, 'vimeo') !== false) {
        // Catch Vimeo iframe
        $provider = 'vimeo';
        $videoId = Extras\get_vimeoId($videoLink);
      }
  }
?>

<?php if(strlen($videoId) && strlen($provider)): ?>
  <article <?php post_class('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6'); ?>>
  <div class="video-post__inner">  
    <div class="video-post__video col-md-6"> 
      <div 
        class="video-post__video__container <?php echo $provider; ?>-player" 
        data-videoid="<?php echo $videoId; ?>"

        <?php if(has_post_thumbnail()): ?>
          data-background="<?php the_post_thumbnail_url(); ?>"
        <?php endif; ?>
      >
         
      </div>
    <!-- video would be here -->
    </div>
    <div class="video-post__content col-md-6">
      <header>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php get_template_part('templates/entry-meta'); ?>
      </header>
      <div class="entry-summary">
        <?php the_excerpt(); ?>
      </div>
      <footer>
      </footer>
    </div>
  </div>
  </article>
<?php else : ?>
  <article <?php post_class('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6'); ?>>
    <header>
      <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-summary">
      <?php the_excerpt(); ?>
    </div>
    <footer>
      <?php get_template_part('templates/elements/author-badge'); ?>
    </footer>
  </article>
<?php endif; ?>
