<section class="sidebar-right">	
  <nav class="nav-right-sidebar  menu-holder">
    <?php
    if (has_nav_menu('sidebar-right_navigation')) :
      wp_nav_menu(['theme_location' => 'sidebar-right_navigation', 'menu_class' => 'nav menu-container']);
    endif;
    ?>
  </nav>
</section>