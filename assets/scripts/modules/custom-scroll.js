function sidebarScroll() {	
	var $sidebars = $(".sidebar-left");

	if ($(window).height() < 620 && aprel.device != 'mobile') {
		$sidebars.mCustomScrollbar({
			axis:"y",
			theme:"dark-thin",
			mouseWheel:{ scrollAmount: 20 },
			scrollInertia: 0,
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true},
			mouseWheel:{ preventDefault: true }
		});
	}	else {
		$sidebars.mCustomScrollbar('destroy');
	}
}
$(window).load(sidebarScroll);
$(document).on( 'aprel:resize', sidebarScroll );