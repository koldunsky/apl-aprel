<article <?php post_class('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6'); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
  <footer>
  	<?php get_template_part('templates/elements/author-badge'); ?>
  </footer>
</article>
