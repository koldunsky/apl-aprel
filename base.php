<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class('preload events_grid_active'); ?>>
    <div class="body-preloader">      
      <?php get_template_part('templates/elements/preloader'); ?>
    </div>
    <div class="site clearfix"  role="document">
      <!--[if IE]>
        <div class="alert alert-warning">
          <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
        </div>
      <![endif]-->
      <?php  
        get_template_part('templates/sidebar-left');
      ?>        
      <?php  
        get_template_part('templates/sidebar-right');
      ?>
      <div class="content-wrap">   
        <?php
          do_action('get_header');
          get_template_part('templates/header');
        ?>
        <div class="content row">
          <main class="main">
            <?php 
              include Wrapper\template_path(); 
            ?>
          </main><!-- /.main -->
        </div><!-- /.content -->
        <?php
          do_action('get_footer');
          get_template_part('templates/footer');
          wp_footer();
        ?>
      </div><!-- /.content-wrap --> 
    </div>
    <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,700,700italic,400italic,300,300italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter41971589 = new Ya.Metrika({ id:41971589, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/41971589" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
  </body>
</html>
