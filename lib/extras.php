<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


/**
 * Count facebook likes
 */


function dez_get_post_share_count($url,$access_token) {
$urls = 'https://graph.facebook.com/?id='. $url.'&access_token='.$access_token;
  $string = @file_get_contents( $urls );
if($string) {
  $fan_count = json_decode( $string,true );
  return $fan_count[likes];
  }
}

function getRusDate($datetime) {
  $yy = (int) substr($datetime,0,4);
  $mm = (int) substr($datetime,5,2);
  $dd = (int) substr($datetime,8,2);
  
  $hours = substr($datetime,11,5);
  
  $month =  array ('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
  
  return ($dd > 0 ? $dd . "&nbsp;" : '') . $month[$mm - 1]."&nbsp;".$yy."г. ";
}

/* 
*
*
* ==== ACF gmap key ===== */
    
function my_acf_google_map_api( $api ){
  
  $api['key'] = 'AIzaSyBUpdFYqXKR53kMHJj4DGJi3aNN0EhaM3k';
  
  return $api;
  
}

add_filter('acf/fields/google_map/api', __NAMESPACE__ . '\\my_acf_google_map_api');



/* 
*
*
* ==== Delete 'Рубрика: ' from categories title ===== */

function filter_category_title($title) {
  return str_replace('Рубрика: ', '', $title);
}
add_filter('get_the_archive_title', __NAMESPACE__ . '\\filter_category_title');


/* 
*
*
* ==== json for events ===== */

function slug_allow_meta( $valid_vars ) {
  $valid_vars = array_merge( $valid_vars, array( 'meta_key', 'meta_value' ) );
  return $valid_vars;
}
add_filter( 'json_query_vars', 'slug_allow_meta' );

/* 
*
*
* ==== Video posts ===== */

function get_vimeoId( $url ) {
  $regex = '~
    # Match Vimeo link and embed code
    (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
    (?:                             # Group vimeo url
        .{0,6}\/\/              # Either http or https or just double slash (//)
        (?:[\w]+\.)*            # Optional subdomains
        vimeo\.com              # Match vimeo.com
        (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
        \/                      # Slash before Id
        ([0-9]+)                # $1: VIDEO_ID is numeric
        [^\s]*                  # Not a space
    )                               # End group
    "?                              # Match end quote if part of src
    (?:[^>]*></iframe>)?            # Match the end of the iframe
    (?:<p>.*</p>)?                  # Match any title information stuff
    ~ix';
  
  preg_match( $regex, $url, $matches );
  
  return $matches[1];
}


/* 
*
*
* ==== Get events function ===== */

function getEvents(
  $post_type = 'post', 
  $date_start, 
  $date_end, 
  $numberposts = -1
)
{
  $events_for_json = array();
  $args = array(
    'numberposts' => $numberposts,
    'post_type'   => $post_type,
    'meta_query'  => array(
      array(
        'key'   => 'event_date_start',
        'compare' => 'BETWEEN',
        'value'   => array($date_start, $date_end),
        'type' => 'DATE'
      )
    )
  );

  // query
  $the_query = new \WP_Query( $args );
  if( $the_query->have_posts() ) { 
    while ( $the_query->have_posts() ) { 
      $the_query->the_post(); 

      $event = array(
      'title' => get_the_title(),
      'url'   => get_the_permalink()
      );


      // get date

      if(get_field('event_date_start')) {
        if (get_field('event_date_start') == get_field('event_date_end')) {
          $event['date'] = get_field('event_date_start');

        } else if(!get_field('event_date_end')) {
          $event['date'] = get_field('event_date_start');

        } else {
          $event['startDate'] = get_field('event_date_start');
          $event['endDate']   = get_field('event_date_end');
        }
      }

      if(get_field('event_time_start')) {
        $event['startTime'] = get_field('event_time_start');
      }

      if(get_field('event_time_end')) {
        $event['endTime'] = get_field('event_time_end');
      }


      // get tags

      $posttags = get_the_tags();
      if ($posttags) {
        foreach($posttags as $tag) {
          $event['tags'][] = $tag -> name; 
        }
      } 


      // get price

      if(get_field('event_price')) {
        $event['price'] = get_field('event_price');
      }


      // get experts

      if(get_field('experts_on_event')) {
        $event['experts'] = get_field('experts_on_event');
      }


      // get registration status

      if(get_field('event_registration_status')) {
        $event['reg_status'] = get_field('event_registration_status');
      }

      
      // get image

      if ( has_post_thumbnail() ) { 
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail', true);
        $event['img'] = $thumb_url[0];
      } 
      $events_for_json[] = $event;
    }
  } 
  return $events_for_json;
}

function get_link_url() { 
  $has_url = get_url_in_content( get_the_content() ); 
  return $has_url ? $has_url : apply_filters( 'the_permalink', get_permalink() ); 
} 