<?php 
use Roots\Sage\Extras;
?>

<?php $postLink = esc_url( Extras\get_link_url() ); ?>

<article 
  <?php post_class('aprel-post-slider-item owl-lazy'); ?> 
  <?php if(has_post_thumbnail()) : ?>
  	data-src="<?php the_post_thumbnail_url( 'full' ); ?>" 
	<?php else: ?>
  	data-src="/wp-content/themes/aprel/static/img/numbers-wallpaper-1920x1200-1024x640.jpg" 
	<?php endif; ?>
  >
  <header>
    <h2 class="entry-title aprel-post-slider-item__title"><a href="<?php echo $postLink; ?>"><?php the_title(); ?></a></h2>
    <?php //the_excerpt(); ?>
    <a href="<?php echo $postLink; ?>">
    	<button class="btn btn-white aprel-post-slider-item__btn">Подробнее</button>
  	</a>
  </header>
</article>