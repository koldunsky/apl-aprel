<?php get_template_part('templates/page', 'header'); ?>
<div class="error-404">
	<div class="error-404__nums">		
		Ошибка 404
	</div>	
	<div class="error-404__text">
		Попробуйте поискать то, что не нашлось:
	</div>
	<?php get_search_form(); ?>
</div>
