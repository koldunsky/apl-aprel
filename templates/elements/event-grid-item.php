<script id="event-grid-item" type="text/template">
    <article class="event-grid__item">
        <section class="event-grid__item__front">  
            <div class="event-grid__item__tags">
                <% if(typeof tags !== 'undefined' && !_.isEmpty(tags)) { %>
                    <%= tags[0] %>
                <% } %>
            </div> 
            <h3 class="event-grid__item__title">
                <%= title %>
            </h3>
            
        </section>
        <section class="event-grid__item__back">
            <% if(typeof startTime !== 'undefined' && !_.isEmpty(startTime)) { %>
                <div class="event-grid__item__time">
                    <h4>Время:</h4> 
                    <%= startTime.substring(0, 5) %>
                    <% if(typeof endTime !== 'undefined' && !_.isEmpty(endTime)) { %>
                    -<%= endTime.substring(0, 5) %>
                    <% } %>
                </div>            
            <% } %>    

            <% if(typeof price !== 'undefined' && !_.isEmpty(price)) { %>
                <div class="event-grid__item__price">
                    <h4>Цена:</h4> 
                    <%= price %>
                </div>           
            <% } %>    

            <div class="event-grid__item__reg-status"> 
                <h4>Регистрация:</h4>                
                <% if(typeof reg_status !== 'undefined' && !_.isEmpty(reg_status)) { %>
                    <%= reg_status['label'] %>
                <% } else { %>
                    Закрыта
                <% } %>
            </div>

            <% if(typeof experts !== 'undefined' && !_.isEmpty(experts)) { %>
                <div class="event-grid__item__experts">
                    <h4>Эксперты:</h4>
                    <ul>
                        <% for(var expert in experts) { %>
                            <li>
                                <a href="<%= experts[expert].guid %>">
                                    <%= experts[expert].post_title %>
                                </a>
                            </li>
                        <% } %>
                    </ul>
                </div>
            <% } %>  

            <% if(typeof img !== 'undefined' && !_.isEmpty(img)) { %>
                <a href="<%= url %>" title="<%= title %>">
                    <div class="event-grid__item__img" style="background-image: url(<%= img %>)">
                    </div> 
                </a>                          
            <% } %>          
        </section>

        <footer class="event-grid__item__footer">
            <a href="<%= url %>" title="<%= title %>">
                <% if( typeof date === 'undefined' ) { %>
                    <%= moment(startDate).format('DD.MM.') %>
                    -                    
                    <%= moment(endDate).format('DD.MM.YYYY') %>
                <% } else { %>
                    <%= moment(date).format('DD.MM.YYYY') %>
                <% } %>
            </a>
        </footer>
    </article>
</script>