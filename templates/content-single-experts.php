<?php while (have_posts()) : the_post(); ?>
	<?php
		// extra fields
		$ev_dt = get_field('event_dt'); //date and time
		$ev_d_start = new DateTime($ev_dt['event_date_start']);
		$ev_d_end = new DateTime($ev_dt['event_date_end']); 
		$location = get_field('event_loc');

	?>
  <article <?php post_class('team-member-page'); ?>>
    <header>
      <div class="team-member__img" style="width: 220px; height: 220px; background-image: url('<?php echo(types_render_field( 'expert_avatar', array('url' => true,'size' => 'full'))); ?>')">
      </div>
      <h1 class="entry-title"><?php the_title(); ?>
			<span class="team-member__soc-links">
				<?php echo(types_render_field( 'team-member_social-links')); ?>			
			</span></h1>
    </header>
    <div class="entry-content">   
			<?php the_content(); ?>
	  </div>
  </article>
  <footer>
  </footer>
<?php endwhile; ?>
